<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m200603_080215_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'bx_user_id' => $this->integer(),
            'active' => $this->tinyInteger()->defaultValue(1),
            'email' => $this->string()->notNull(),
            'name' => $this->string()->null(),
            'last_name' => $this->string()->null(),
            'auth_key' => $this->string()->null(),
            'access_token' => $this->string()->null(),
            'password' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->null(),
            'role' => $this->integer()->defaultValue(40),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime()
        ]);

        $this->insert('user', [
            'bx_user_id' => 0,
            'active' => 2,
            'name' => 'Admin',
            'last_name' => 'Admin L',
            'auth_key' => Yii::$app->security->generateRandomString(32),
            'password' => Yii::$app->security->generatePasswordHash('123123'),
            'email' => 'admin@admin.com',
            'role' => 20,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
