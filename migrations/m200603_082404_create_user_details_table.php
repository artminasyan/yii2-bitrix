<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_details}}`.
 */
class m200603_082404_create_user_details_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_details}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'second_name' => $this->string()->null(),
            'personal_gender' => $this->integer()->defaultValue(0),
            'personal_profession' => $this->string()->null(),
            'personal_www' => $this->string()->null(),
            'personal_birthday' => $this->date()->null(),
            'personal_photo' => $this->string()->null(),
            'personal_icq' => $this->string()->null(),
            'personal_phone' => $this->string()->null(),
            'personal_fax' => $this->string()->null(),
            'personal_mobile' => $this->string()->null(),
            'personal_pager' => $this->string()->null(),
            'personal_street' => $this->string()->null(),
            'personal_city' => $this->string()->null(),
            'personal_state' => $this->string()->null(),
            'personal_zip' => $this->string()->null(),
            'personal_country' => $this->string()->null(),
            'work_company' => $this->string()->null(),
            'work_position' => $this->string()->null(),
            'work_phone' => $this->string()->null(),
            'uf_interests' => $this->string()->null(),
            'uf_skills' => $this->string()->null(),
            'uf_sites' => $this->string()->null(),
            'uf_xing' => $this->string()->null(),
            'uf_linkedin' => $this->string()->null(),
            'uf_facebook' => $this->string()->null(),
            'uf_twitter' => $this->string()->null(),
            'uf_skype' => $this->string()->null(),
            'uf_district' => $this->string()->null(),
            'uf_phone_inner' => $this->integer()->null(),
            'user_type' => $this->string()->defaultValue('employee'),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime()
        ]);

        $this->addForeignKey('fk-user_id', '{{%user_details}}', 'user_id', 'user', 'id');

        $this->insert('user_details', [
            'user_id' => 1,
            'personal_photo' => '/img/user/1/user-default-avatar.svg',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-user_id', '{{%user_details}}');
        $this->dropTable('{{%user_details}}');
    }
}
