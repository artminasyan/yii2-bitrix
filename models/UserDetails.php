<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "user_details".
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $second_name
 * @property int|null $personal_gender
 * @property string|null $personal_profession
 * @property string|null $personal_www
 * @property string|null $personal_birthday
 * @property string|null $personal_photo
 * @property string|null $personal_icq
 * @property string|null $personal_phone
 * @property string|null $personal_fax
 * @property string|null $personal_mobile
 * @property string|null $personal_pager
 * @property string|null $personal_street
 * @property string|null $personal_city
 * @property string|null $personal_state
 * @property string|null $personal_zip
 * @property string|null $personal_country
 * @property string|null $work_company
 * @property string|null $work_position
 * @property string|null $work_phone
 * @property string|null $uf_interests
 * @property string|null $uf_skills
 * @property string|null $uf_sites
 * @property string|null $uf_xing
 * @property string|null $uf_linkedin
 * @property string|null $uf_facebook
 * @property string|null $uf_twitter
 * @property string|null $uf_skype
 * @property string|null $uf_district
 * @property int|null $uf_phone_inner
 * @property string|null $user_type
 * @property string|null $updated_at
 * @property string|null $created_at
 *
 * @property User $user
 */
class UserDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'personal_gender', 'uf_phone_inner'], 'integer'],
            [['personal_birthday', 'updated_at', 'created_at'], 'safe'],
            [['second_name', 'personal_profession', 'personal_www', 'personal_photo', 'personal_icq', 'personal_phone', 'personal_fax', 'personal_mobile', 'personal_pager', 'personal_street', 'personal_city', 'personal_state', 'personal_zip', 'personal_country', 'work_company', 'work_position', 'work_phone', 'uf_interests', 'uf_skills', 'uf_sites', 'uf_xing', 'uf_linkedin', 'uf_facebook', 'uf_twitter', 'uf_skype', 'uf_district', 'user_type'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'second_name' => 'Second Name',
            'personal_gender' => 'Personal Gender',
            'personal_profession' => 'Personal Profession',
            'personal_www' => 'Personal Www',
            'personal_birthday' => 'Personal Birthday',
            'personal_photo' => 'Personal Photo',
            'personal_icq' => 'Personal Icq',
            'personal_phone' => 'Personal Phone',
            'personal_fax' => 'Personal Fax',
            'personal_mobile' => 'Personal Mobile',
            'personal_pager' => 'Personal Pager',
            'personal_street' => 'Personal Street',
            'personal_city' => 'Personal City',
            'personal_state' => 'Personal State',
            'personal_zip' => 'Personal Zip',
            'personal_country' => 'Personal Country',
            'work_company' => 'Work Company',
            'work_position' => 'Work Position',
            'work_phone' => 'Work Phone',
            'uf_interests' => 'Uf Interests',
            'uf_skills' => 'Uf Skills',
            'uf_sites' => 'Uf Sites',
            'uf_xing' => 'Uf Xing',
            'uf_linkedin' => 'Uf Linkedin',
            'uf_facebook' => 'Uf Facebook',
            'uf_twitter' => 'Uf Twitter',
            'uf_skype' => 'Uf Skype',
            'uf_district' => 'Uf District',
            'uf_phone_inner' => 'Uf Phone Inner',
            'user_type' => 'User Type',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }
    /**
     * @return array|array[]
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes'=>[
                    self::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    self::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }


    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
