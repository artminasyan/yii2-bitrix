<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserDetails;

/**
 * UserDetailsSearch represents the model behind the search form of `app\models\UserDetails`.
 */
class UserDetailsSearch extends UserDetails
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'personal_gender', 'uf_phone_inner'], 'integer'],
            [['second_name', 'personal_profession', 'personal_www', 'personal_birthday', 'personal_photo', 'personal_icq', 'personal_phone', 'personal_fax', 'personal_mobile', 'personal_pager', 'personal_street', 'personal_city', 'personal_state', 'personal_zip', 'personal_country', 'work_company', 'work_position', 'work_phone', 'uf_interests', 'uf_skills', 'uf_sites', 'uf_xing', 'uf_linkedin', 'uf_facebook', 'uf_twitter', 'uf_skype', 'uf_district', 'user_type', 'updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserDetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'personal_gender' => $this->personal_gender,
            'personal_birthday' => $this->personal_birthday,
            'uf_phone_inner' => $this->uf_phone_inner,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'second_name', $this->second_name])
            ->andFilterWhere(['like', 'personal_profession', $this->personal_profession])
            ->andFilterWhere(['like', 'personal_www', $this->personal_www])
            ->andFilterWhere(['like', 'personal_photo', $this->personal_photo])
            ->andFilterWhere(['like', 'personal_icq', $this->personal_icq])
            ->andFilterWhere(['like', 'personal_phone', $this->personal_phone])
            ->andFilterWhere(['like', 'personal_fax', $this->personal_fax])
            ->andFilterWhere(['like', 'personal_mobile', $this->personal_mobile])
            ->andFilterWhere(['like', 'personal_pager', $this->personal_pager])
            ->andFilterWhere(['like', 'personal_street', $this->personal_street])
            ->andFilterWhere(['like', 'personal_city', $this->personal_city])
            ->andFilterWhere(['like', 'personal_state', $this->personal_state])
            ->andFilterWhere(['like', 'personal_zip', $this->personal_zip])
            ->andFilterWhere(['like', 'personal_country', $this->personal_country])
            ->andFilterWhere(['like', 'work_company', $this->work_company])
            ->andFilterWhere(['like', 'work_position', $this->work_position])
            ->andFilterWhere(['like', 'work_phone', $this->work_phone])
            ->andFilterWhere(['like', 'uf_interests', $this->uf_interests])
            ->andFilterWhere(['like', 'uf_skills', $this->uf_skills])
            ->andFilterWhere(['like', 'uf_sites', $this->uf_sites])
            ->andFilterWhere(['like', 'uf_xing', $this->uf_xing])
            ->andFilterWhere(['like', 'uf_linkedin', $this->uf_linkedin])
            ->andFilterWhere(['like', 'uf_facebook', $this->uf_facebook])
            ->andFilterWhere(['like', 'uf_twitter', $this->uf_twitter])
            ->andFilterWhere(['like', 'uf_skype', $this->uf_skype])
            ->andFilterWhere(['like', 'uf_district', $this->uf_district])
            ->andFilterWhere(['like', 'user_type', $this->user_type]);

        return $dataProvider;
    }
}
