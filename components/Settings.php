<?php

namespace app\components;

use yii\base\Component;

class Settings extends Component
{

    /**
     * Bitrix24 client ID
     *
     * @return mixed
     */
    public function clientId() {
        return \Yii::$app->params['BXClientID'];
    }

    /**
     * Bitrix24 client Secret
     *
     * @return mixed
     */
    public function clientSecret() {
        return \Yii::$app->params['BXClientSecret'];
    }

    /**
     * Current encoding
     *
     * @return mixed
     */
    public function currentEncoding() {
        return \Yii::$app->params['BXCurrentEncoding'];
    }

    /**
     * Ignore SSL
     *
     * @return mixed
     */
    public function ignoreSSL() {
        return \Yii::$app->params['BXIgnoreSSL'];
    }

    /**
     * Log directory
     *
     * @return mixed
     */
    public function logDir() {
        return \Yii::$app->params['BXLogsDir'];
    }

}