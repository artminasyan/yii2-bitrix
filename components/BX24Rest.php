<?php

namespace app\components;

use yii\base\Component;

class BX24Rest extends Component
{

    /**
     * Retrieves filtered list of users. This method will return all users, except: bots, users for e-mail and users for Open Channels.
     *
     * @param $id
     * @return array|mixed|string|string[]
     */
    public function userFindById($id) {
        if (!empty($id)) {
            return CRest::call('user.get', ['ID' => $id]);
        } else {
            return json_encode(['error' => 'ID cannot be empty.']);
        }
    }

    /************************************ Deal Category ************************************/

    /**
     * Возвращает направление сделок по идентификатору.
     *
     * @param $id | Идентификатор направления.
     * @return array|mixed|string|string[]
     */
    public function dealCategoryFindById($id) {
        if (!empty($id)) {
            return CRest::call('crm.dealcategory.get', ['ID' => $id]);
        } else {
            return json_encode(['error' => 'ID cannot be empty.']);
        }
    }

    /**
     * Возвращает список направлений сделок по фильтру. Является реализацией списочного метода для направлений сделок.
     *
     * @return array|mixed|string|string[]
     */
    public function dealCategoryList() {
        return CRest::call('crm.dealcategory.list');
    }

    /**
     * Возвращает описание полей направления сделок.
     *
     * @return array|mixed|string|string[]
     */
    public function dealCategoryFields() {
        return CRest::call('crm.dealcategory.fields');
    }

    /**
     * Создаёт новое направление сделок.
     *
     * @param array $params
     * @return array|false|mixed|string|string[]
     */
    public function dealCategoryAdd($params) {
        if (!empty($params)) {
            return CRest::call('crm.dealcategory.add', [
                'FIELDS' => $params
            ]);
        } else {
            return json_encode(['error' => 'Params cannot be empty.']);
        }
    }

    /**
     * Обновляет существующее направление.
     *
     * @param $id | Идентификатор направления.
     * @param array $params
     * @return array|false|mixed|string|string[]
     */
    public function dealCategoryUpdate($id, $params = []) {
        if (!empty($id)) {
            return CRest::call('crm.dealcategory.update', [
                'ID'     => $id,
                'FIELDS' => $params
            ]);
        } else {
            return json_encode(['error' => 'ID cannot be empty.']);
        }
    }

    /**
     * Удаляет направление сделок.
     *
     * @param $id | Идентификатор направления.
     * @return array|mixed|string|string[]
     */
    public function dealCategoryDelete($id) {
        if (!empty($id)) {
            return CRest::call('crm.dealcategory.delete', ['ID' => $id]);
        } else {
            return json_encode(['error' => 'ID cannot be empty.']);
        }

    }

    /**
     * Метод чтения настроек общего направления сделок.
     *
     * @return array|mixed|string|string[]
     */
    public function dealCategoryDefaultSet() {
        return CRest::call('crm.dealcategory.default.set');
    }

    /**
     * Метод записи настроек общего направления сделок.
     *
     * @return array|mixed|string|string[]
     */
    public function dealCategoryDefaultGet() {
        return CRest::call('crm.dealcategory.default.get');
    }

    /**
     * Возвращает список стадий сделок для направления по идентификатору. Равносилен вызову метода crm.status.list с параметором ENTITY_ID равным результату вызова crm.dealcategory.status.
     *
     * @return array|mixed|string|string[]
     */
    public function dealCategoryStageList() {
        return CRest::call('crm.dealcategory.stage.list');
    }

    /**
     * Возвращает идентификатор типа справочника для хранения стадий по идентификатору направления сделок.
     *
     * @param $id | Идентификатор направления.
     * @return array|mixed|string|string[]
     */
    public function dealCategoryStatus($id) {
        return CRest::call('crm.dealcategory.status', ['ID' => $id]);
    }

    /************************************ Currency & Currency Localizations ************************************/

    /**
     * Возвращает валюту по символьному идентификатору.
     *
     * @param $id | Символьный идентификатор валюты.
     * @return array|false|mixed|string|string[]
     */
    public function currencyFindById($id) {
        if (!empty($id)) {
            return CRest::call('crm.currency.get', ['ID' => $id]);
        } else {
            return json_encode(['error' => 'ID cannot be empty.']);
        }
    }

    /**
     * Возвращает описание полей валюты.
     *
     * @return array|mixed|string|string[]
     */
    public function currencyFields() {
        return CRest::call('crm.currency.fields');
    }

    /**
     * Возвращает список валют. Является реализацией списочного метода для валют. Обратите внимание, что в данной реализации параметры "filter", "select" и "navigation" не поддерживаются.
     *
     * @return array|mixed|string|string[]
     */
    public function currencyList() {
        return CRest::call('crm.currency.list');
    }

    /**
     * Создаёт новую валюту.
     *
     * @param $fields | Набор полей - массив вида array("поле"=>"значение"[, ...]), содержащий значения полей валюты, где "поле" может принимать значения из возвращаемых методом crm.currency.fields.
     * @return array|false|mixed|string|string[]
     */
    public function currencyAdd($fields) {
        if (!empty($fields)) {
            if (array_key_exists('CURRENCY', $fields)) {

                return CRest::call('crm.currency.add', [
                    'FIELDS' => $fields
                ]);

            } else {
                return json_encode(['error' => 'Currency ID cannot be empty.']);
            }
        } else {
            return json_encode(['error' => 'Specify one or more fields. Fields cannot be empty.']);
        }
    }

    /**
     * @param $id | Символьный идентификатор валюты.
     * @param $fields | массив вида array("обновляемое поле"=>"значение"[, ...]), где "обновляемое поле" может принимать значения из возвращаемых методом crm.currency.fields.
     * @return array|false|mixed|string|string[]
     */
    public function currencyUpdate($id, $fields) {
        if (!empty($fields)) {
            if (array_key_exists('CURRENCY', $fields)) {

                return CRest::call('crm.currency.add', [
                    'ID' => $id,
                    'FIELDS' => $fields
                ]);

            } else {
                return json_encode(['error' => 'Currency ID cannot be empty.']);
            }
        } else {
            return json_encode(['error' => 'Specify one or more fields. Fields cannot be empty.']);
        }
    }

    /**
     * Удаляет валюту.
     *
     * @param $id | Символьный идентификатор валюты.
     * @return array|false|mixed|string|string[]
     */
    public function currencyDelete($id) {
        if (!empty($id)) {
            return CRest::call('crm.currency.delete', ['ID' => $id]);
        } else {
            return json_encode(['error' => 'ID cannot be empty.']);
        }
    }

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/currency/crm_currency_base_get.php
     *
     * Метод позволяет получить символьный идентификатор базовой валюты.
     *
     * @return array|mixed|string|string[]
     */
    public function crmCurrencyBaseGet() {
        return CRest::call('crm.currency.base.get');
    }

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/currency/crm_currency_base_set.php
     *
     * Метод устанавливает валюту в качестве базовой. Если попробовать сделать базовой валюту, которая и так ей является - вернется null. Иначе - true/false (успех или ошибка).
     *
     * @param $id | Символьный код валюты, которую необходимо сделать базовой
     * @return array|false|mixed|string|string[]
     */
    public function crmCurrencyBaseSet($id) {
        if (!empty($id)) {
            return CRest::call('crm.currency.base.set', [
                'ID' => $id
            ]);
        } else {
            return json_encode(['error' => 'Currency ID cannot be empty.']);
        }
    }

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/currency/crm_currency_localizations_fields.php
     *
     * Возвращает описание локализаций для валюты.
     *
     * @return array|mixed|string|string[]
     */
    public function crmCurrencyLocalizationFields() {
        return CRest::call('crm.currency.localizations.fields');
    }

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/currency/crm_currency_localizations_get.php
     *
     * Возвращает локализации для валюты, указанной по символьному идентификатору.
     *
     * @param $id | Символьный идентификатор валюты.
     * @return array|false|mixed|string|string[]
     */
    public function crmCurrencyLocalizationFindById($id) {
        if (!empty($id)) {
            return CRest::call('crm.currency.localizations.get', [
                'ID' => $id
            ]);
        } else {
            return json_encode(['error' => 'Currency ID cannot be empty.']);
        }
    }

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/currency/crm_currency_localizations_set.php
     *
     * Устанавливает локализации для валюты, указанной по символьному идентификатору.
     *
     * @param $id | Символьный идентификатор валюты.
     * @param $locales | Набор локализаций - массив вида array("язык" => array("поле"=>"значение"[, ...])), содержащий значения полей локализаций, где "язык" - идентификатор языка, "поле" - одно из возвращаемых методом crm.currency.localizations.fields значений.
     * @return array|false|mixed|string|string[]
     */
    public function currencyLocalizationSet($id, $locales) {
        if (!empty($id)) {
            if (!empty($locales)) {
                return CRest::call('crm.currency.localizations.set', [
                    'ID' => $id,
                    'LOCALIZATIONS' => $locales
                ]);
            } else {
                return json_encode(['error' => 'Specify one or more locale. Locales cannot be empty.']);
            }
        } else {
            return json_encode(['error' => 'Currency ID cannot be empty.']);
        }
    }

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/currency/crm_currency_localizations_delete.php
     *
     * Удаляет выбранные локализации для валюты, указанной по символьному идентификатору.
     *
     * @param $id | Символьный идентификатор валюты.
     * @param $lids | Массив идентификаторов языков, локализации которых требуется удалить.
     * @return array|false|mixed|string|string[]
     */
    public function crmCurrencyLocalizationDelete($id, $lids) {
        if (!empty($id)) {
            if (!empty($lids)) {
                return CRest::call('crm.currency.localizations.delete', [
                    'ID' => $id,
                    'LIDS' => $lids
                ]);
            } else {
                return json_encode(['error' => 'Specify one or more locale. Locales cannot be empty.']);
            }
        } else {
            return json_encode(['error' => 'Currency ID cannot be empty.']);
        }
    }

    /************************************ Catalog ************************************/

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/catalog/crm_catalog_fields.php
     *
     * Возвращает описание полей каталога товаров.
     *
     * @return array|mixed|string|string[]
     */
    public function crmCatalogFields() {
        return CRest::call('crm.catalog.fields');
    }

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/catalog/crm_catalog_get.php
     *
     * Возвращает товарный каталог по идентификатору.
     *
     * @param $id | Идентификатор товарного каталога.
     * @return array|false|mixed|string|string[]
     */
    public function crmCatalogFindById($id) {
        if (!empty($id)) {
            return CRest::call('crm.catalog.get', ['ID' => $id]);
        } else {
            return json_encode(['error' => 'ID cannot be empty.']);
        }
    }

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/catalog/crm_catalog_list.php
     *
     * Возвращает список товарных каталогов. Является реализацией списочного метода для товарных каталогов.
     *
     * @return array|mixed|string|string[]
     */
    public function crmCatalogList() {
        return CRest::call('crm.catalog.list');
    }

    /************************************ CRM Contact ************************************/

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/contacts/crm_contact_fields.php
     *
     * Возвращает описание полей контакта, в том числе пользовательских.
     *
     * @return array|mixed|string|string[]
     */
    public function crmContactFields() {
        return CRest::call('crm.contact.fields');
    }

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/contacts/crm_contact_list.php
     *
     * Возвращает список контактов по фильтру. Является реализацией списочного метода для контактов.
     *
     * @return array|mixed|string|string[]
     */
    public function crmContactList() {
        return CRest::call('crm.contact.list');
    }

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/contacts/crm_contact_get.php
     *
     * Возвращает контакт по идентификатору.
     * Для получения списка компаний, привязанных к контакту используйте метод crm.contact.company.items.get.
     *
     * @param $id | Идентификатор контакта.
     * @return array|false|mixed|string|string[]
     */
    public function crmContactFindById($id) {
        if (!empty($id) && !is_null($id)) {
            return CRest::call('crm.contact.get', ['ID' => $id]);
        } else {
            return json_encode(['error' => 'ID cannot be empty.']);
        }
    }

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/contacts/crm_contact_add.php
     *
     * Создаёт новый контакт.
     *
     * @param $fields | Набор полей - массив вида array("поле"=>"значение"[, ...]), содержащий значения полей контакта.
     * @param array $params | Набор параметров. REGISTER_SONET_EVENT - произвести регистрацию события добавления контакта в живой ленте. Дополнительно будет отправлено уведомление ответственному за контакт.
     * @return array|false|mixed|string|string[]
     */
    public function crmContactAdd($fields, $params = []) {
        if (!empty($fields)) {
            return CRest::call('crm.contact.add', [
                'FIELDS' => $fields,
                'PARAMS' => $params
            ]);
        } else {
            return json_encode(['error' => 'Specify one or more field. Field(s) cannot be empty.']);
        }
    }

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/contacts/crm_contact_update.php
     *
     * Обновляет существующий контакт.
     * Важно! Настоятельно рекомендуется при обновлении адреса передавать полный набор полей адреса в метод обновления. Особенности обновления полей адреса описаны здесь.
     *
     * @param $id | Идентификатор контакта.
     * @param $fields | Набор полей - массив вида array("обновляемое поле"=>"значение"[, ...]), где "обновляемое поле" может принимать значения из возвращаемых методом crm.contact.fields.
     * @param $params | Набор параметров. REGISTER_SONET_EVENT - произвести регистрацию события изменения контакта в живой ленте. Дополнительно будет отправлено уведомление ответственному за контакт.
     * @return array|false|mixed|string|string[]
     */
    public function crmContactUpdate($id, $fields, $params) {
        if (!empty($id) && !is_null($id)) {
            if (!empty($fields) && !is_null($fields)) {
                return CRest::call('crm.contact.update', [
                    'ID' => $id,
                    'FIELDS' => $fields,
                    'PARAMS' => $params
                ]);
            } else {
                return json_encode(['error' => 'Specify one or more field. Field(s) cannot be empty.']);
            }
        } else {
            return json_encode(['error' => 'ID cannot be empty.']);
        }
    }

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/contacts/crm_contact_company_delete.php
     *
     * Удаляет контакт и все связанные с ним объекты.
     *
     * @param $id | Идентификатор контакта.
     * @return array|false|mixed|string|string[]
     */
    public function crmContactDelete($id) {
        if (!empty($id) && !is_null($id)) {
            return CRest::call('crm.contact.delete', ['ID' => $id]);
        } else {
            return json_encode(['error' => 'ID cannot be empty.']);
        }
    }

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/contacts/crm_contact_company_fields.php
     *
     * Возвращает для связи контакт-компания описание полей, используемых методами семейства crm.contact.company.*, то есть crm.contact.company.items.get, crm.contact.company.items.set, crm.contact.company.add и т.д.
     *
     * @return array|mixed|string|string[]
     */
    public function crmContactCompanyFields() {
        return CRest::call('crm.contact.company.fields');
    }

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/contacts/crm_contact_company_add.php
     *
     * Добавляет компанию к указанному контакту.
     *
     * @param $id | Идентификатор контакта.
     * @param $fields | Объект со следующими полями:
     *                      COMPANY_ID - идентификатор компании (обязательное поле)
     *                      SORT - индекс сортировки
     *                      IS_PRIMARY - флаг первичной компании
     * @return array|false|mixed|string|string[]
     */
    public function crmContactCompanyAdd($id, $fields) {
        if (!empty($id) && !is_null($id)) {
            if (!empty($fields) && !is_null($fields)) {
                if (array_key_exists('COMPANY_ID', $fields) || array_key_exists('company_id', $fields)) {
                    return CRest::call('crm.contact.company.add', [
                        'ID' => $id,
                        'FIELDS' => $fields
                    ]);
                } else {
                    return json_encode(['error' => 'COMPANY_ID cannot be empty.']);
                }
            } else {
                return json_encode(['error' => 'Specify one or more field. Field(s) cannot be empty.']);
            }
        } else {
            return json_encode(['error' => 'ID cannot be empty.']);
        }
    }

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/contacts/crm_contact_company_delete.php
     *
     * Удаляет компанию из указанного контакта.
     *
     * @param $id | Идентификатор контакта.
     * @param $fields | Объект со следующими полями:
     *                      COMPANY_ID - идентификатор компании (обязательное поле)
     * @return array|false|mixed|string|string[]
     */
    public function crmContactCompanyDelete($id, $fields) {
        if (!empty($id) && !is_null($id)) {
            if (!empty($fields)) {
                if (array_key_exists('COMPANY_ID', $fields) || array_key_exists('company_id', $fields)) {
                    return CRest::call('crm.contact.company.delete', [
                        'ID' => $id,
                        'FIELDS' => $fields
                    ]);
                } else {
                    return json_encode(['error' => 'COMPANY_ID cannot be empty.']);
                }
            } else {
                return json_encode(['error' => 'Specify one or more field. Field(s) cannot be empty.']);
            }
        } else {
            return json_encode(['error' => 'ID cannot be empty.']);
        }
    }

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/contacts/crm_contact_company_items_get.php
     *
     * Возвращает набор компаний, связанных с указанным контактом.
     *
     * @param $id | Идентификатор контакта.
     * @return array|false|mixed|string|string[]
     */
    public function crmContactCompanyItemsFindById($id) {
        if (!empty($id) && !is_null($id)) {
            return CRest::call('crm.contact.company.items.get', ['ID' => $id]);
        } else {
            return json_encode(['error' => 'ID cannot be empty.']);
        }
    }

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/contacts/crm_contact_company_items_delete.php
     *
     * Очищает набор компаний, связанных с указанным контактом.
     *
     * @param $id | Идентификатор контакта
     * @return array|false|mixed|string|string[]
     */
    public function crmContactCompanyItemsDelete($id) {
        if (!empty($id) && !is_null($id)) {
            return CRest::call('crm.contact.company.items.delete', ['ID' => $id]);
        } else {
            return json_encode(['error' => 'ID cannot be empty.']);
        }
    }

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/contacts/crm_contact_company_items_set.php
     *
     * Устанавливает набор компаний, связанных с указанным контактом.
     *
     * @param $id | Идентификатор контакта.
     * @param $fields | Набор компаний в виде массива объектов со следующими полями:
     *                      COMPANY_ID - идентификатор компании (обязательное поле)
     *                      SORT - индекс сортировки
     *                      IS_PRIMARY - флаг первичной компании
     * @return array|false|mixed|string|string[]
     */
    public function crmContactCompanyItemsSet($id, $fields) {
        if (!empty($id) && !is_null($id)) {
            if (!empty($fields)) {
                if (array_key_exists('COMPANY_ID', $fields) || array_key_exists('company_id', $fields)) {
                    return CRest::call('crm.contact.company.items.set', [
                        'ID' => $id,
                        'FIELDS' => $fields
                    ]);
                } else {
                    return json_encode(['error' => 'COMPANY_ID cannot be empty.']);
                }
            } else {
                return json_encode(['error' => 'Specify one or more field. Field(s) cannot be empty.']);
            }
        } else {
            return json_encode(['error' => 'ID cannot be empty.']);
        }
    }

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/contacts/crm_contact_userfield_list.php
     *
     * Возвращает пользовательское поле контактов по идентификатору.
     *
     * @param array $order | Поля сортировки.
     * @param array $filter | Поля фильтра.
     * @return array|mixed|string|string[]
     */
    public function crmContactUserFieldList($order = [], $filter = []) {
        return CRest::call('crm.contact.userfield.list', [
            'ORDER' => $order,
            'FILTER' => $filter
        ]);
    }

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/contacts/crm_contact_userfield_get.php
     *
     * Возвращает пользовательское поле контактов по идентификатору.
     *
     * @param $id | Идентификатор пользовательского поля.
     * @return array|false|mixed|string|string[]
     */
    public function crmContactUserFieldFindById($id) {
        if (!empty($id) && !is_null($id)) {
            return CRest::call('crm.contact.userfield.get', ['ID' => $id]);
        } else {
            return json_encode(['error' => 'ID cannot be empty.']);
        }
    }

    /**
     * Link: https://dev.1c-bitrix.ru/rest_help/crm/contacts/crm_contact_userfield_add.php
     *
     * Создаёт новое пользовательское поле для контактов.
     * Системное ограничение на название поля - 20 знаков. К названию пользовательского поля всегда добавляется префикс UF_CRM_, то есть реальная длина названия - 13 знаков.
     *
     * @param $fields | Набор полей - массив вида array("поле"=>"значение"[, ...]), содержащий описание пользовательского поля.
     * @param $list | Содержит набор значений списка для пользовательских полей типа Список. Указывается при создании/обновлении поля.
     * @return array|false|mixed|string|string[]
     */
    public function crmContactUserFieldAdd($fields, $list) {
        if (!empty($fields) && !is_null($fields)) {
            if (!empty($list) && !is_null($list)) {
                return CRest::call('crm.contact.userfield.add', [
                    'FIELDS' => $fields,
                    'LIST' => $list
                ]);
            } else {
                return json_encode(['error' => 'Specify one or more list. List(s) cannot be empty.']);
            }
        } else {
            return json_encode(['error' => 'Specify one or more field. Field(s) cannot be empty.']);
        }
    }

    /************************************ CRM / Режим работы ************************************/

    /**
     * Возвращает текущие настройки режима работы CRM:
     * 1 - классический режим работы (с лидами).
     * 2 - режим работы без лидов.
     *
     * То есть метод crm.settings.mode.get возвращает значение, определённое в crm.enum.settings.mode.
     *
     * @return array|mixed|string|string[]
     */
    public function modeOfOperation() {
        return CRest::call('crm.settings.mode.get');
    }

    /************************************ CRM / Автоматизация ************************************/

    /**
     * Активирует триггер Webhook, настроенный в автоматизации CRM.
     *
     * @param $target | Целевой объект для автоматизации, указывается в виде TYPENAME_ID (например, LEAD_25).
     * @param $code | Уникальный символьный код триггера, настроенного в Автоматизации на конкретный статус/стадию документа.
     * @return array|false|mixed|string|string[]
     */
    public function crmAutomationTrigger($target, $code) {
        if (!empty($target)) {
            return CRest::call('crm.automation.trigger', [
                'TARGET' => $target,
                'CODE' => $code
            ]);
        } else {
            return json_encode(['error' => 'Target cannot be empty.']);
        }
    }

    /**
     * Метод добавляет триггер. Возвращает true или ошибку с описанием.
     *
     * @param $code | Внутренний уникальный (в рамках приложения) идентификатор триггера. Должен соответствовать маске [a-z0-9\.\-_]
     * @param $name | Название триггера
     * @return array|false|mixed|string|string[]
     */
    public function addAutomationTrigger($code, $name) {
        if (!empty($code) && !empty($name)) {
            return CRest::call('crm.automation.trigger.add', [
                'CODE' => $code,
                'NAME' => $name
            ]);
        } else {
            return json_encode(['error' => 'Trigger code or name cannot be empty.']);
        }
    }

    /**
     * Метод удаляет триггер. Возвращает true или ошибку с описанием.
     *
     * @param $code | Внутренний уникальный (в рамках приложения) идентификатор триггера. Должен соответствовать маске [a-z0-9\.\-_]
     * @return array|false|mixed|string|string[]
     */
    public function deleteAutomationTrigger($code) {
        if (!empty($code)) {
            return CRest::call('crm.automation.trigger.delete', [
                'CODE' => $code
            ]);
        } else {
            return json_encode(['error' => 'Trigger code cannot be empty.']);
        }
    }

    /**
     * Метод для получения списка приложений и триггеров. Возвращает массив добавленных приложением триггеров с полями NAME и CODE.
     *
     * @return array|mixed|string|string[]
     */
    public function getAutomationTriggerList() {
        return CRest::call('crm.automation.trigger.list');
    }

    public function automationTriggerExecute() {
        return CRest::call('crm.automation.trigger.execute');
        // Not finished
    }

}