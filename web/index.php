<?php

// comment out the following two lines when deployed to production
use app\components\Debug;

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';

// Dump data
function dd() {
    array_map(function($x) { Debug::var_dump($x); }, func_get_args()); die;
}

(new yii\web\Application($config))->run();
