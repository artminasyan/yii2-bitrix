<?php

return [
    // Set default page for show in crm route
    '/crm' => '/deal/default/index',

    // Catalog routes
    '/crm/catalog' => '/catalog/catalog/index',
    '/crm/catalog/create' => '/catalog/catalog/create/index',
    '/crm/catalog/view/<id:\d+>' => '/catalog/catalog/view',
    '/crm/catalog/update/<id:\d+>' => '/catalog/catalog/update',

    // Deal Category routes
    '/crm/dealcategory' => '/dealcategory/default/index',
    '/crm/dealcategory/create' => '/dealcategory/default/create/index',
    '/crm/dealcategory/view/<id:\d+>' => '/dealcategory/default/view',
    '/crm/dealcategory/update/<id:\d+>' => '/dealcategory/default/update',

    // Currency routes
    '/crm/currency' => '/currency/currency/index',
    '/crm/currency/create' => '/currency/currency/create',
    '/crm/currency/view/<id:\d+>' => '/currency/currency/view',
    '/crm/currency/update/<id:\d+>' => '/currency/currency/update',
];
