<?php

return [
    'languages' => [
        'ru' => 'ru',
        'en' => 'en',
    ],

    'bsVersion' => '4.x', // this will set globally `bsVersion` to Bootstrap 4.x for all Krajee Extensions

    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',

    // Bitrix24 config
    'BXWebhookUrl' => 'https://b24-hsa1g7.bitrix24.ru/rest/1/5pnpqron4ebhqjty/',
    'BXClientID' => '',
    'BXClientSecret' => '',

    'BXCurrentEncoding' => 'utf-8',
    'BXIgnoreSSL' => false, //turn off validate ssl by curl
    'BXLogTypeDump' => true, //logs save var_export for viewing convenience
    'BXBlockLog' => true, //turn off default logs
    'BXLogsDir' => $_SERVER['DOCUMENT_ROOT'].'/logs/', //directory path to save the log

    // ATS
    'atsPrivateKey' => ''
];
