<?php

namespace app\modules\crm\dealcategory\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\crm\dealcategory\models\DealCategory;

/**
 * DealCategorySearch represents the model behind the search form of `app\modules\crm\dealcategory\models\DealCategory`.
 */
class DealCategorySearch extends DealCategory
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'bx_dealcategory_id', 'is_default', 'is_locked', 'sort'], 'integer'],
            [['name', 'status', 'updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DealCategory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'bx_dealcategory_id' => $this->bx_dealcategory_id,
            'is_default' => $this->is_default,
            'is_locked' => $this->is_locked,
            'sort' => $this->sort,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
