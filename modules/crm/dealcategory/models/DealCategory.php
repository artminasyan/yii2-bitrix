<?php

namespace app\modules\crm\dealcategory\models;

use app\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "deal_category".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $bx_dealcategory_id
 * @property string|null $name
 * @property int|null $is_default
 * @property int|null $is_locked
 * @property int|null $sort
 * @property string|null $status
 * @property string|null $updated_at
 * @property string|null $created_at
 *
 * @property User $user
 */
class DealCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'deal_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'bx_dealcategory_id', 'is_default', 'is_locked', 'sort'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['name', 'status'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'bx_dealcategory_id' => 'Bx Dealcategory ID',
            'name' => 'Name',
            'is_default' => 'Is Default',
            'is_locked' => 'Is Locked',
            'sort' => 'Sort',
            'status' => 'Status',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return array|array[]
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes'=>[
                    self::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    self::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
