<?php

namespace app\modules\crm\dealcategory;

use yii\base\Module;

/**
 * dealcategory module definition class
 */
class DealCategory extends Module
{

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\crm\dealcategory\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
