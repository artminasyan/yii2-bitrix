<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%deal_category}}`.
 */
class m200604_001814_create_deal_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%deal_category}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'bx_dealcategory_id' => $this->integer(),
            'name' => $this->string(),
            'is_default' => $this->integer()->defaultValue(0),
            'is_locked' => $this->integer()->defaultValue(0),
            'sort' => $this->integer()->defaultValue(0),
            'status_code' => $this->string()->defaultValue('DEAL_STAGE'),
            'status' => $this->integer()->defaultValue(0),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime()
        ]);

        $this->addForeignKey('fk-deal_category_user_id', '{{%deal_category}}', 'user_id', 'user', 'id');

        $this->insert('{{%deal_category}}', [
            'user_id' => 1,
            'bx_dealcategory_id' => 0,
            'name' => 'Обшее',
            'is_default' => 1
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-deal_category_user_id', '{{%deal_category}}');
        $this->dropTable('{{%deal_category}}');
    }
}
