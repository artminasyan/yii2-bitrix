<?php

namespace app\modules\crm\deal\controllers;

use yii\web\Controller;

/**
 * Default controller for the `deal` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
