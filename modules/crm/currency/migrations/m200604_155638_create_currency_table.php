<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%currency}}`.
 */
class m200604_155638_create_currency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%currency}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'bx_currency_id' => $this->integer(),
            'amount' => $this->float(),
            'amount_cnt' => $this->integer(),
            'currency' => $this->string()->notNull(),
            'base' => $this->integer()->defaultValue(0),
            'date_update' => $this->dateTime(),
            'dec_point' => $this->string()->defaultValue('.'),
            'decimals' => $this->integer()->defaultValue(2),
            'sort' => $this->integer()->defaultValue(0),
            'hide_zero' => $this->integer()->defaultValue(0),
            'thousands_variant' => $this->string()->defaultValue('C'),
            'thousands_separator' => $this->string(),
            'lid' => $this->string(),
            'status' => $this->integer()->defaultValue(0),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime()
        ]);

        // USD Default
        $this->insert('{{%currency}}', [
            'bx_currency_id' => 2,
            'user_id' => 1,
            'currency' => 'USD',
            'amount' => 68.7900,
            'amount_cnt' => 1,
            'sort' => 200,
            'lid' => 'ru',
            'dec_point' => '.',
            'decimals' => 2,
            'date_update' => date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        // Ruble Default
        $this->insert('{{%currency}}', [
            'bx_currency_id' => 1,
            'user_id' => 1,
            'currency' => 'RUB',
            'amount' => 1.0000,
            'amount_cnt' => 1,
            'sort' => 100,
            'base' => 1,
            'lid' => 'ru',
            'dec_point' => '.',
            'decimals' => 2,
            'date_update' => date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        // Euro Default
        $this->insert('{{%currency}}', [
            'bx_currency_id' => 3,
            'user_id' => 1,
            'currency' => 'EUR',
            'amount' => 78.3200,
            'amount_cnt' => 1,
            'sort' => 300,
            'lid' => 'ru',
            'dec_point' => '.',
            'decimals' => 2,
            'date_update' => date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        // UAH Default
        $this->insert('{{%currency}}', [
            'bx_currency_id' => 4,
            'user_id' => 1,
            'currency' => 'UAH',
            'amount' => 25.1100,
            'amount_cnt' => 10,
            'sort' => 400,
            'lid' => 'ru',
            'dec_point' => '.',
            'decimals' => 2,
            'date_update' => date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        // BYN Default
        $this->insert('{{%currency}}', [
            'bx_currency_id' => 5,
            'user_id' => 1,
            'currency' => 'BYN',
            'amount' => 32.2000,
            'amount_cnt' => 1,
            'sort' => 500,
            'lid' => 'ru',
            'dec_point' => '.',
            'decimals' => 2,
            'date_update' => date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        $this->createTable('{{%currency_localizations}}', [
            'id' => $this->primaryKey(),
            'bx_currency_id' => $this->integer(),
            'user_id' => $this->integer()->notNull(),
            'parent_id' => $this->integer()->null(),
            'currency_id' => $this->string()->notNull(),
            'language' => $this->string(),
            'format_string' => $this->string(),
            'full_name' => $this->string(),
            'status' => $this->integer()->defaultValue(0),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime()
        ]);

        // USD ru
        $this->insert('{{%currency_localizations}}', [
            'bx_currency_id' => 2,
            'user_id' => 1,
            'parent_id' => 1,
            'currency_id' => 'USD',
            'language' => 'ru',
            'format_string' => '$#',
            'full_name' => 'Доллар США',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        // USD en
        $this->insert('{{%currency_localizations}}', [
            'bx_currency_id' => 2,
            'user_id' => 1,
            'parent_id' => 1,
            'currency_id' => 'USD',
            'language' => 'en',
            'format_string' => '$#',
            'full_name' => 'US Dollar',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        // RUB ru
        $this->insert('{{%currency_localizations}}', [
            'bx_currency_id' => 1,
            'user_id' => 1,
            'parent_id' => 2,
            'currency_id' => 'RUB',
            'language' => 'ru',
            'format_string' => '# руб.',
            'full_name' => 'Российский рубль',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        // RUB en
        $this->insert('{{%currency_localizations}}', [
            'bx_currency_id' => 1,
            'user_id' => 1,
            'parent_id' => 2,
            'currency_id' => 'RUB',
            'language' => 'en',
            'format_string' => '# rub.',
            'full_name' => 'Russian Ruble',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        // EUR ru
        $this->insert('{{%currency_localizations}}', [
            'bx_currency_id' => 3,
            'user_id' => 1,
            'parent_id' => 3,
            'currency_id' => 'EUR',
            'language' => 'ru',
            'format_string' => '# &euro;',
            'full_name' => 'Евро',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        // EUR en
        $this->insert('{{%currency_localizations}}', [
            'bx_currency_id' => 3,
            'user_id' => 1,
            'parent_id' => 3,
            'currency_id' => 'EUR',
            'language' => 'en',
            'format_string' => '# &euro;',
            'full_name' => 'Euro',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        // UAH ru
        $this->insert('{{%currency_localizations}}', [
            'bx_currency_id' => 4,
            'user_id' => 1,
            'parent_id' => 4,
            'currency_id' => 'UAH',
            'language' => 'ru',
            'format_string' => '# грн.',
            'full_name' => 'Гривна',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        // UAH en
        $this->insert('{{%currency_localizations}}', [
            'bx_currency_id' => 4,
            'user_id' => 1,
            'parent_id' => 4,
            'currency_id' => 'UAH',
            'language' => 'en',
            'format_string' => '# грн.',
            'full_name' => 'Hryvnia',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        // BYN ru
        $this->insert('{{%currency_localizations}}', [
            'bx_currency_id' => 5,
            'user_id' => 1,
            'parent_id' => 5,
            'currency_id' => 'BYN',
            'language' => 'ru',
            'format_string' => '# руб.',
            'full_name' => 'Белорусский рубль',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        // BYN en
        $this->insert('{{%currency_localizations}}', [
            'bx_currency_id' => 5,
            'user_id' => 1,
            'parent_id' => 5,
            'currency_id' => 'BYN',
            'language' => 'en',
            'format_string' => '# руб.',
            'full_name' => 'Belarusian Ruble',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);



        $this->addForeignKey('fk-currency_user_id', 'currency', 'user_id', 'user', 'id');
        $this->addForeignKey('fk-currency_localization_user_id', 'currency_localizations', 'user_id', 'user', 'id');
        $this->addForeignKey('fk-currency_localization_parent_id', 'currency_localizations', 'parent_id', 'currency', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-currency_user_id', '{{%currency}}');
        $this->dropForeignKey('fk-currency_localization_user_id', '{{%currency_localizations}}');
        $this->dropForeignKey('fk-currency_localization_parent_id', '{{%currency_localizations}}');
        $this->dropTable('{{%currency}}');
        $this->dropTable('{{%currency_localizations}}');
    }
}
