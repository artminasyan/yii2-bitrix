<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\crm\currency\models\CurrencyLocalizations */

$this->title = Yii::t('app', 'Create Currency Localizations');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Currency Localizations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-localizations-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
