<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\crm\currency\models\Currency */

$this->title = Yii::t('app', 'Create Currency');
?>
<div class="currency-create">

    <!-- CRM Menu -->
    <?= Yii::$app->view->render('@app/views/layouts/crm-menu'); ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
