<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\crm\currency\models\Currency */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="currency-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-row">
        <div class="col-sm-12 col-md-4">
            <?= $form->field($model, 'currency')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-12 col-md-4">
            <?= $form->field($model, 'amount')->textInput() ?>
        </div>
        <div class="col-sm-12 col-md-4">
            <?= $form->field($model, 'amount_cnt')->textInput() ?>
        </div>
    </div>

    <div class="form-row">
        <div class="col-sm-12 col-md-4">
            <?= $form->field($model, 'dec_point')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-12 col-md-4">
            <?= $form->field($model, 'decimals')->textInput() ?>
        </div>
        <div class="col-sm-12 col-md-4">
            <?= $form->field($model, 'sort')->textInput() ?>
        </div>
    </div>

    <div class="form-row">
        <div class="col-sm-12 col-md-4">
            <?= $form->field($model, 'thousands_variant')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-12 col-md-4">
            <?= $form->field($model, 'thousands_separator')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-12 col-md-4">
            <?= $form->field($model, 'lid')->widget(\kartik\select2\Select2::className(), [
                'data' => [
                    'ru' => 'RU',
                    'en' => 'EN'
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'options' => [
                    'placeholder' => 'Select Language'
                ]
            ]) ?>
        </div>
    </div>

    <hr>

    <div class="form-row">
        <div class="col-sm-12 col-md-6">
            <?= $form->field($model, 'format_string_ru')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-12 col-md-6">
            <?= $form->field($model, 'format_string_en')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="form-row">
        <div class="col-sm-12 col-md-6">
            <?= $form->field($model, 'full_name_ru')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-12 col-md-6">
            <?= $form->field($model, 'full_name_en')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <hr>

    <div class="form-row">
        <div class="col-sm-12 col-md-3">
            <?= $form->field($model, 'hide_zero')->checkbox() ?>
        </div>
        <div class="col-sm-12 col-md-3">
            <?= $form->field($model, 'base')->checkbox() ?>
        </div>
        <div class="col-sm-12 col-md-6 text-right">
            <div class="form-group">
                <?= Html::a(Yii::t('app', 'Cancel'), '/crm/currency', ['class' => 'btn btn-light w-25']) ?>
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary w-25']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
