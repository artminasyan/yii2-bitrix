<?php

use app\models\User;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\crm\currency\models\CurrencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Currencies');
?>
<div class="currency-index">

    <!-- CRM Menu -->
    <?= Yii::$app->view->render('@app/views/layouts/crm-menu'); ?>

    <div class="row mb-3">
        <div class="col-sm-12 col-md-6">
            <h3><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="col-sm-12 col-md-6 text-right">
            <h3><?= Html::a(Yii::t('app', 'Create Currency'), ['create'], ['class' => 'btn btn-success']) ?></h3>
        </div>
    </div>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- Module content -->
    <?= GridView::widget([
        'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'ExpandRowColumn' => [
                'class' => 'kartik\grid\ExpandRowColumn',
                'value' => function ($model, $key, $index,$column) {
                    return GridView::ROW_COLLAPSED;
                },
                'detailUrl'=> Yii::$app->request->getBaseUrl().'/expand-view',
            ],
            ['class' => 'kartik\grid\CheckboxColumn'],
            'id',
            [
                'attribute' => 'user_id',

                'filter' => \yii\helpers\ArrayHelper::map(User::find()->where(['active' => User::STATUS_ACTIVE])->asArray()->all(), 'id', 'name'),
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['prompt' => ''],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],

                'value' => function ($model, $key, $index, $column) {
                    $user = \app\models\User::findOne($model->user_id);
                    $name = '';

                    if (!empty($user['name']) && empty($user['last_name'])) {
                        $name .= $user['name'];
                    } else if (!empty($user['last_name']) && empty($user['name'])) {
                        $name .= $user['last_name'];
                    } else if (!empty($user['name']) && !empty($user['last_name'])) {
                        $name .= $user['name'].' '.$user['last_name'];
                    } else {
                        $name .= $user['email'];
                    }
                    return $name;
                }
            ],
            'amount',
            'amount_cnt',
            'currency',
            'base',
            //'date_update',
            //'dec_point',
            //'decimals',
            'sort',
            //'hide_zero',
            //'thousands_variant',
            //'thousands_separator',
            //'lid',
            //'updated_at',
            //'created_at',

            [
                'class' => 'kartik\grid\ActionColumn',
                'dropdown' => true,
                'urlCreator' => function($action, $model, $key, $index) {
                    return Url::to([$action, 'id' => $key]);
                },
                'viewOptions' => ['title' => 'View', 'data-toggle' => 'tooltip'],
                'updateOptions' => ['title' => 'Update', 'data-toggle' => 'tooltip'],
                'deleteOptions' => ['title' => 'Delete', 'data-toggle' => 'tooltip'],
            ]
        ], // check the configuration for grid columns by clicking button above
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' =>  [
            [
                'content' =>
                    Html::a('<i class="fas fa-plus"></i>', ['/crm/currency/create'], [
                        'class' => 'btn btn-success',
                        'title' => Yii::t('app', 'Create Currency'),
                        'data-pjax' => 0,
                    ]) . ' '.
                    Html::a('<i class="fas fa-redo"></i>', ['grid-demo'], [
                        'class' => 'btn btn-outline-secondary',
                        'title' => Yii::t('app', 'Reset Grid')
                    ]),
                'options' => ['class' => 'btn-group mr-2']
            ],
            '{export}',
            '{toggleData}',
        ],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        // parameters from the demo form
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
        ],
        'responsive' => true,
        'hover' => true,
        'bordered' => true,
        'striped' => true,
        'showPageSummary' => false,
//        'floatHeader' => true,
        'condensed' => false,
        'toggleDataOptions' => ['minCount' => 10],
        'itemLabelSingle' => 'currency',
        'itemLabelPlural' => 'currencies',
        'persistResize' => true,
        'resizableColumns'=>true,
        'resizeStorageKey'=>Yii::$app->user->id . '-' . date("m")
    ]); ?>

    <?php Pjax::end(); ?>

</div>
