<?php

namespace app\modules\crm\currency\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\crm\currency\models\Currency;

/**
 * CurrencySearch represents the model behind the search form of `app\modules\crm\currency\models\Currency`.
 */
class CurrencySearch extends Currency
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'amount_cnt', 'decimals', 'sort', 'base', 'hide_zero'], 'integer'],
            [['amount'], 'number'],
            [['currency', 'date_update', 'dec_point', 'thousands_variant', 'thousands_separator', 'lid', 'updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Currency::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'amount' => $this->amount,
            'amount_cnt' => $this->amount_cnt,
            'date_update' => $this->date_update,
            'decimals' => $this->decimals,
            'sort' => $this->sort,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'base', $this->base])
            ->andFilterWhere(['like', 'dec_point', $this->dec_point])
            ->andFilterWhere(['like', 'hide_zero', $this->hide_zero])
            ->andFilterWhere(['like', 'thousands_variant', $this->thousands_variant])
            ->andFilterWhere(['like', 'thousands_separator', $this->thousands_separator])
            ->andFilterWhere(['like', 'lid', $this->lid]);

        return $dataProvider;
    }
}
