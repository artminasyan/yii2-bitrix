<?php

namespace app\modules\crm\currency\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\crm\currency\models\CurrencyLocalizations;

/**
 * CurrencyLocalizationsSearch represents the model behind the search form of `app\modules\crm\currency\models\CurrencyLocalizations`.
 */
class CurrencyLocalizationsSearch extends CurrencyLocalizations
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'parent_id'], 'integer'],
            [['currency_id', 'language', 'format_string', 'full_name', 'updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CurrencyLocalizations::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'parent_id' => $this->parent_id,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'currency_id', $this->currency_id])
            ->andFilterWhere(['like', 'language', $this->lang_id])
            ->andFilterWhere(['like', 'format_string', $this->format_string])
            ->andFilterWhere(['like', 'full_name', $this->full_name]);

        return $dataProvider;
    }
}
