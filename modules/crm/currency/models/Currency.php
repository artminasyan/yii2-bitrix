<?php

namespace app\modules\crm\currency\models;

use app\models\User;
use omgdef\multilingual\MultilingualBehavior;
use omgdef\multilingual\MultilingualQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "currency".
 *
 * @property int $id
 * @property int $user_id
 * @property float|null $amount
 * @property int|null $amount_cnt
 * @property string $currency
 * @property int|null $base
 * @property string|null $date_update
 * @property string|null $dec_point
 * @property int|null $decimals
 * @property int|null $sort
 * @property int|null $hide_zero
 * @property string|null $thousands_variant
 * @property string|null $thousands_separator
 * @property string|null $lid
 * @property string|null $updated_at
 * @property string|null $created_at
 *
 * @property User $user
 * @property CurrencyLocalizations[] $currencyLocalizations
 */
class Currency extends \yii\db\ActiveRecord
{
    public $currency_id;
    public $language;
    public $format_string;
    public $full_name;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'currency'], 'required'],
            [['user_id', 'amount_cnt', 'decimals', 'sort', 'base', 'hide_zero'], 'integer'],
            [['amount'], 'number'],
            [['date_update', 'updated_at', 'created_at', 'currency_id', 'language', 'format_string', 'full_name'], 'safe'],
            [['currency', 'dec_point', 'thousands_variant', 'thousands_separator', 'lid'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'amount' => 'Amount',
            'amount_cnt' => 'Amount Cnt',
            'currency' => 'Currency',
            'base' => 'Base',
            'date_update' => 'Date Update',
            'dec_point' => 'Dec Point',
            'decimals' => 'Decimals',
            'sort' => 'Sort',
            'hide_zero' => 'Hide Zero',
            'thousands_variant' => 'Thousands Variant',
            'thousands_separator' => 'Thousands Separator',
            'lid' => 'Lid',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return array|array[]
     */
    public function behaviors()
    {
        return [
            'ml' => [
                'class' => MultilingualBehavior::className(),
                'languages' => Yii::$app->params['languages'],
                'requireTranslations' => true,
                'langClassName' => CurrencyLocalizations::className(),
                'defaultLanguage' => 'ru',
                'langForeignKey' => 'parent_id',
                'tableName' => "{{%currency_localizations}}",
                'attributes' => [
                    'format_string',
                    'full_name'
                ]
            ],
            [
                'class' => TimestampBehavior::className(),
                'attributes'=>[
                    self::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    self::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return MultilingualQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return new MultilingualQuery(get_called_class());
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Gets query for [[CurrencyLocalizations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyLocalizations()
    {
        return $this->hasMany(CurrencyLocalizations::className(), ['parent_id' => 'id']);
    }

    public static function saveCurency($params) {
        print_r($params);die;
        if (!empty($params)) {
            $findCurrency = self::find()->select('currency')->where(['currency' => $params['currency']])->one();

            $dateTime = date('Y-m-d H:i:s');

            if (empty($findCurrency['currency'])) {
                $currency = new Currency();
            } else {
                $currency = Currency::findOne(['currency' => $params['currency']]);
            }
            $currency->user_id = Yii::$app->user->id;
            $currency->amount = $params['amount'];
            $currency->amount_cnt = $params['amount_cnt'];
            $currency->currency = $params['currency'];
            $currency->base = $params['base'];
            $currency->date_update = $dateTime;
            $currency->dec_point = $params['dec_point'];
            $currency->decimals = $params['decimals'];
            $currency->sort = $params['sort'];
            $currency->hide_zero = $params['hide_zero'];
            $currency->thousands_variant = $params['thousands_variant'];
            $currency->thousands_separator = $params['thousands_separator'];
            $currency->lid = $params['lid'];

            if ($currency->save()) {
                $findCurrencyLocale = CurrencyLocalizations::find()->multilingual()->select('currency_id')->where(['currency_id' => $params['currency']])->one();


                if (empty($findCurrencyLocale['currency_id'])) {
                    $currencyLocalization = new CurrencyLocalizations();
                } else {
                    $currencyLocalization = CurrencyLocalizations::findOne(['currency_id' => $params['currency']]);
                }
                $currencyLocalization->user_id = Yii::$app->user->id;
                $currencyLocalization->parent_id = $currency->id;
                $currencyLocalization->currency_id = $currency->currency;
                $currencyLocalization->currency_id = $currency->currency;
            }
        }
    }
}
