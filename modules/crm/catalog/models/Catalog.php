<?php

namespace app\modules\crm\catalog\models;

use app\models\User;
use Yii;

/**
 * This is the model class for table "catalog".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $bx_catalog_id
 * @property int|null $originator_id
 * @property int|null $origin_id
 * @property int|null $xml_id
 * @property int|null $deleted
 * @property string|null $updated_at
 * @property string|null $created_at
 *
 * @property User $user
 */
class Catalog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'catalog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'bx_catalog_id', 'originator_id', 'origin_id', 'xml_id', 'deleted'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'bx_catalog_id' => Yii::t('app', 'Bx Catalog ID'),
            'originator_id' => Yii::t('app', 'Originator ID'),
            'origin_id' => Yii::t('app', 'Origin ID'),
            'xml_id' => Yii::t('app', 'Xml ID'),
            'deleted' => Yii::t('app', 'Deleted'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
