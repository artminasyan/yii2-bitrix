<?php

namespace app\modules\crm\catalog\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\crm\catalog\models\Catalog;

/**
 * CatalogSearch represents the model behind the search form about `app\modules\crm\catalog\models\Catalog`.
 */
class CatalogSearch extends Catalog
{
    public function rules()
    {
        return [
            [['id', 'user_id', 'bx_catalog_id', 'originator_id', 'origin_id', 'xml_id', 'deleted'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Catalog::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'bx_catalog_id' => $this->bx_catalog_id,
            'originator_id' => $this->originator_id,
            'origin_id' => $this->origin_id,
            'xml_id' => $this->xml_id,
            'deleted' => $this->deleted,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ]);

        return $dataProvider;
    }
}
