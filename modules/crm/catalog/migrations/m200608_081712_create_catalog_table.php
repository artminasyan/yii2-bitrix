<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%catalog}}`.
 */
class m200608_081712_create_catalog_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%catalog}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'bx_catalog_id' => $this->integer(),
            'originator_id' => $this->integer(),
            'origin_id' => $this->integer(),
            'xml_id' => $this->integer(),
            'deleted' => $this->integer()->defaultValue(0),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime(),
        ]);

        $this->addForeignKey('fk-catalog_user_id', '{{%catalog}}', 'user_id', 'user', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-catalog_user_id', '{{%catalog}}');
        $this->dropTable('{{%catalog}}');
    }
}
