<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\modules\crm\catalog\models\Catalog $model
 */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Catalog',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Catalogs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalog-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
