<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\modules\crm\catalog\models\Catalog $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="catalog-form">

    <?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 2,
        'attributes' => [

            'originator_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Originator ID...']],

            'origin_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Origin ID...']],

            'xml_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Xml ID...']],

            'deleted' => ['type' => Form::INPUT_CHECKBOX, 'options' => ['placeholder' => 'Enter Deleted...']],

        ]

    ]);

    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'),
        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
    );
    ActiveForm::end(); ?>

</div>
