<?php

return [
    'lead' => 'Lead',
    'deal' => 'Deal',
    'contact' => 'Contact',
    'company' => 'Company',
    'product' => 'Products',
    'catalog' => 'Catalog',
    'currency' => 'Currency',
    'deal_category' => 'Deal Category',
    'more' => 'More',
];
