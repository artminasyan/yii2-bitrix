<?php

return [
    'lead' => 'Лиды',
    'deal' => 'Сделки',
    'contact' => 'Контакты',
    'company' => 'Компании',
    'product' => 'Товары',
    'catalog' => 'Catalog',
    'currency' => 'Currency',
    'deal_category' => 'Deal Category',
    'more' => 'More',
];
