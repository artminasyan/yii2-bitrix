<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UserDetails */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-details-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'second_name',
            'personal_gender',
            'personal_profession',
            'personal_www',
            'personal_birthday',
            'personal_photo',
            'personal_icq',
            'personal_phone',
            'personal_fax',
            'personal_mobile',
            'personal_pager',
            'personal_street',
            'personal_city',
            'personal_state',
            'personal_zip',
            'personal_country',
            'work_company',
            'work_position',
            'work_phone',
            'uf_interests',
            'uf_skills',
            'uf_sites',
            'uf_xing',
            'uf_linkedin',
            'uf_facebook',
            'uf_twitter',
            'uf_skype',
            'uf_district',
            'uf_phone_inner',
            'user_type',
            'updated_at',
            'created_at',
        ],
    ]) ?>

</div>
