<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserDetailsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-details-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'second_name') ?>

    <?= $form->field($model, 'personal_gender') ?>

    <?= $form->field($model, 'personal_profession') ?>

    <?php // echo $form->field($model, 'personal_www') ?>

    <?php // echo $form->field($model, 'personal_birthday') ?>

    <?php // echo $form->field($model, 'personal_photo') ?>

    <?php // echo $form->field($model, 'personal_icq') ?>

    <?php // echo $form->field($model, 'personal_phone') ?>

    <?php // echo $form->field($model, 'personal_fax') ?>

    <?php // echo $form->field($model, 'personal_mobile') ?>

    <?php // echo $form->field($model, 'personal_pager') ?>

    <?php // echo $form->field($model, 'personal_street') ?>

    <?php // echo $form->field($model, 'personal_city') ?>

    <?php // echo $form->field($model, 'personal_state') ?>

    <?php // echo $form->field($model, 'personal_zip') ?>

    <?php // echo $form->field($model, 'personal_country') ?>

    <?php // echo $form->field($model, 'work_company') ?>

    <?php // echo $form->field($model, 'work_position') ?>

    <?php // echo $form->field($model, 'work_phone') ?>

    <?php // echo $form->field($model, 'uf_interests') ?>

    <?php // echo $form->field($model, 'uf_skills') ?>

    <?php // echo $form->field($model, 'uf_sites') ?>

    <?php // echo $form->field($model, 'uf_xing') ?>

    <?php // echo $form->field($model, 'uf_linkedin') ?>

    <?php // echo $form->field($model, 'uf_facebook') ?>

    <?php // echo $form->field($model, 'uf_twitter') ?>

    <?php // echo $form->field($model, 'uf_skype') ?>

    <?php // echo $form->field($model, 'uf_district') ?>

    <?php // echo $form->field($model, 'uf_phone_inner') ?>

    <?php // echo $form->field($model, 'user_type') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
