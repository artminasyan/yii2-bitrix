<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-details-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'second_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'personal_gender')->textInput() ?>

    <?= $form->field($model, 'personal_profession')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'personal_www')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'personal_birthday')->textInput() ?>

    <?= $form->field($model, 'personal_photo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'personal_icq')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'personal_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'personal_fax')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'personal_mobile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'personal_pager')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'personal_street')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'personal_city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'personal_state')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'personal_zip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'personal_country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'work_company')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'work_position')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'work_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uf_interests')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uf_skills')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uf_sites')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uf_xing')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uf_linkedin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uf_facebook')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uf_twitter')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uf_skype')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uf_district')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'uf_phone_inner')->textInput() ?>

    <?= $form->field($model, 'user_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
