<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-details-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create User Details', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'second_name',
            'personal_gender',
            'personal_profession',
            //'personal_www',
            //'personal_birthday',
            //'personal_photo',
            //'personal_icq',
            //'personal_phone',
            //'personal_fax',
            //'personal_mobile',
            //'personal_pager',
            //'personal_street',
            //'personal_city',
            //'personal_state',
            //'personal_zip',
            //'personal_country',
            //'work_company',
            //'work_position',
            //'work_phone',
            //'uf_interests',
            //'uf_skills',
            //'uf_sites',
            //'uf_xing',
            //'uf_linkedin',
            //'uf_facebook',
            //'uf_twitter',
            //'uf_skype',
            //'uf_district',
            //'uf_phone_inner',
            //'user_type',
            //'updated_at',
            //'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
