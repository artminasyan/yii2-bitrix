<?php

use app\components\CRest;

$result = CRest::installApp();

if($result['rest_only'] === false) {

    if ($result['install'] === true) {

        $this->registerJsFile('//api.bitrix24.com/api/v1/');
        $this->registerJs(
            "BX24.init(function() { BX24.installFinish(); });",
            \yii\web\View::POS_READY
        );

        echo 'Installation has been finished';
    } else {
        echo 'Installation error';
    }

}?>