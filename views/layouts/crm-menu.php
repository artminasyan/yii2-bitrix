<?php

use yii\bootstrap4\Nav;
?>
<div class="row mb-4">
    <div class="col-md-12">
        <?= Nav::widget([
            'route' => ['class' => 'text-white'],
            'items' => [
                [
                    'label' => Yii::t('app', 'deal'),
                    'url' => ['/crm/deal'],
                    'linkOptions' => [
                        'class' => 'text-dark'
                    ],
                ],
                [
                    'label' => Yii::t('app', 'lead'),
                    'url' => ['/crm/lead'],
                    'linkOptions' => [
                        'class' => 'text-dark'
                    ],
                ],
                [
                    'label' => Yii::t('app', 'contact'),
                    'url' => ['/crm/contact'],
                    'linkOptions' => [
                        'class' => 'text-dark'
                    ],
                ],
                [
                    'label' => Yii::t('app', 'company'),
                    'url' => ['/crm/company'],
                    'linkOptions' => [
                        'class' => 'text-dark'
                    ],
                ],
                [
                    'label' => Yii::t('app', 'product'),
                    'url' => ['/crm/products'],
                    'linkOptions' => [
                        'class' => 'text-dark'
                    ],
                ],
                [
                    'label' => Yii::t('app', 'more'),
                    'linkOptions' => [
                        'class' => 'text-dark'
                    ],
                    'items' => [
                        ['label' => Yii::t('app', 'catalog'), 'url' => '/crm/catalog'],
                        ['label' => Yii::t('app', 'deal_category'), 'url' => '/crm/dealcategory'],
                        ['label' => Yii::t('app', 'currency'), 'url' => '/crm/currency'],
                    ],
                ],
            ],
            'options' => ['class' =>'nav-pills bg-light px-4 py-2'], // set this to nav-tab to get tab-styled navigation
        ]);
        ?>
    </div>
</div>