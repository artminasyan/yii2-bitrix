<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-white sidebar collapse">
    <div class="sidebar-sticky pt-3">
        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>CRUD</span>
            <a class="d-flex align-items-center text-muted" href="#" aria-label="Add a new report">
                <span data-feather="plus-circle"></span>
            </a>
        </h6>

        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link active" href="/crm">
                    <span data-feather="home"></span>
                    CRM <span class="sr-only">(current)</span>
                </a>
            </li>
        </ul>

        <ul class="nav flex-column mb-2">

        </ul>
    </div>
</nav>